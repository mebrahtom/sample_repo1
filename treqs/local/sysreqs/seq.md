```puml
@startuml Sample sequence diagram 
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response
Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
@enduml
```

![Generated diagram](http://www.plantuml.com/plantuml/svg/XS_13S8m38NXUwPu2sG1X53R4M02qreGAN5AxEoF2vnGfHwjz-cN70OrQjPBnetAcXdAhyuoCvP4ZqO5OSp9ptZ1LEydZDsUB9PcibG5rush1TyGtNpmZjArYl9_Iwgp1jflVl4vnWFzQmyiYpyItW00)


