# Meeting Scheduler System

System requirements for a an online food ordering system

## Detailed system requirements

The following requirements are derived from high-level system requirements

### [requirement id=REQ0006 parent=REQ0002 quality=QR0002 test=TC0001]
### [requirement id=RESR_0031 parent=REQ0002 quality=QR0205 test=TC0013]

The system must provide the ability to place an order, book a delivery with our partners to the user.

### [requirement id=REQ0007 parent=REQ0002]

The system must provide option to pay in card/swish/klarna

### [requirement id=REQ0008 parent=REQ0002]

The system should provide ability to send a confirmation by email and text message.

### [requirement id=REQ0009 parent=REQ0003 test=TC0003]

The system should allow user to add notes to the seller for a special order.
[requirement id=REQ009 parent=REQ0003 test=TC0003] 

### [requirement id=REQ0010 parent=REQ0003 test=TC0004]

The system should do something

### [requirement id=SR_0028]

This is a dummy requirement.
